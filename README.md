CordyChain Consortium Project
Overview
The CordyChain Consortium Project implements a blockchain network using Hyperledger Fabric. It defines organizations, orderers, and peers in the network configuration to facilitate decentralized applications (dApps) within the consortium.

Prerequisites
Before getting started with the CordyChain Consortium Project, ensure you have the following prerequisites installed on your system:

Docker
Hyperledger Fabric
Installation
Follow these steps to install and set up the CordyChain Consortium Project:

Clone this repository:

git clone https://github.com/your_username/cordychain-consortium.git


Navigate to the project directory:

cd cordychain-consortium

Usage
1. Configure the Network
Modify the crypto-config.yaml file to define the organizations, orderers, and peers according to your network requirements. Below is an example of a crypto-config.yaml file:

OrdererOrgs:
- Name: Orderer
 Domain: cordychain.com
 EnableNodeOUs: true
 Specs:
   - Hostname: orderer

PeerOrgs:
- Name: cordychain
 Domain: cordychain.com
 EnableNodeOUs: true
 Specs:
   - Hostname: bt.cordychain.com
     CommonName: bt.cordychain.com
 Users:
   Count: 1

# Add configurations for other organizations, orderers, and peers as needed
##############################################################################################################################################
# Sets up the 3 orgs in the network
# CAPABILITIES
Capabilities:
  Application: &ApplicationCapabilities
    V2_0: true
  Orderer: &OrdererCapabilities
    V2_0: true
  Channel: &ChannelCapabilities
    V2_0: true

# ORGANIZATIONS
Organizations:
  - &Orderer
    Name: Orderer
    ID: OrdererMSP
    MSPDir: ./crypto-config/ordererOrganizations/cordychain.com/msp
    Policies: &OrdererPolicies
      Readers:
        Type: Signature
        Rule: "OR('OrdererMSP.member')"
      Writers:
        Type: Signature
        Rule: "OR('OrdererMSP.member')"
      Admins:
        Type: Signature
        Rule: "OR('OrdererMSP.admin')"
      Endorsement:
        Type: Signature
        Rule: "OR('OrdererMSP.member')"

  - &cordychain
    Name: cordychain
    ID: cordychainMSP
    MSPDir: ./crypto-config/peerOrganizations/cordychain.com/msp
    Policies: &CordychainPolicies
      Readers:
        Type: Signature
        Rule: "OR('cordychainMSP.member')"
      Writers:
        Type: Signature
        Rule: "OR('cordychainMSP.member')"
      Admins:
        Type: Signature
        Rule: "OR('cordychainMSP.admin')"
      Endorsement:
        Type: Signature
        Rule: "OR('cordychainMSP.member')"
    AnchorPeers:
      - Host: bt.cordychain.com
        Port: 7051

  - &BCEL
    Name: BCEL
    ID: BCELMSP
    MSPDir: ./crypto-config/peerOrganizations/bcel.com/msp
    Policies: &BCELPolicies
      Readers:
        Type: Signature
        Rule: "OR('BCELMSP.member')"
      Writers:
        Type: Signature
        Rule: "OR('BCELMSP.member')"
      Admins:
        Type: Signature
        Rule: "OR('BCELMSP.admin')"
      Endorsement:
        Type: Signature
        Rule: "OR('BCELMSP.member')"
    AnchorPeers:
      - Host: bt.bcel.com
        Port: 7051
      - Host: bcm.bcel.com
        Port: 7051

  - &Harvesters
    Name: Harvesters
    ID: HarvestersMSP
    MSPDir: ./crypto-config/peerOrganizations/Harvesters.com/msp
    Policies: &HarvestersPolicies
      Readers:
        Type: Signature
        Rule: "OR('HarvestersMSP.member')"
      Writers:
        Type: Signature
        Rule: "OR('HarvestersMSP.member')"
      Admins:
        Type: Signature
        Rule: "OR('HarvestersMSP.member')"
      Endorsement:
        Type: Signature
        Rule: "OR('HarvestersMSP.member')"
    AnchorPeers:
      - Host: vil.Harvesters.com
        Port: 7051

  - &RegulatoryBody
    Name: RegulatoryBody
    ID: RegulatoryBodyMSP
    MSPDir: ./crypto-config/peerOrganizations/RegulatoryBody.com/msp
    Policies: &RegulatoryBodyPolicies
      Readers:
        Type: Signature
        Rule: "OR('RegulatoryBodyMSP.member')"
      Writers:
        Type: Signature
        Rule: "OR('RegulatoryBodyMSP.member')"
      Admins:
        Type: Signature
        Rule: "OR('RegulatoryBodyMSP.member')"
      Endorsement:
        Type: Signature
        Rule: "OR('RegulatoryBodyMSP.member')"
    AnchorPeers:
      - Host: gov.RegulatoryBody.com
        Port: 7051

# ORDERER
Orderer: &OrdererDefaults
  OrdererType: solo
  Addresses:
    - orderer.cordychain.com:7050
  BatchTimeout: 2s
  BatchSize:
    MaxMessageCount: 10
    AbsoluteMaxBytes: 98 MB
    PreferredMaxBytes: 512 KB
  Policies:
    Readers:
      Type: ImplicitMeta
      Rule: "ANY Readers"
    Writers:
      Type: ImplicitMeta
      Rule: "ANY Writers"
    Admins:
      Type: ImplicitMeta
      Rule: "ANY Admins"
    BlockValidation:
      Type: ImplicitMeta
      Rule: "ANY Writers"
  Capabilities:
    <<: *OrdererCapabilities

# APPLICATION
Application: &ApplicationDefaults
  ACLs: &ACLsDefault
    lscc/ChaincodeExists: /Channel/Application/Readers
    lscc/GetDeploymentSpec: /Channel/Application/Readers
    lscc/GetChaincodeData: /Channel/Application/Readers
    lscc/GetInstantiatedChaincodes: /Channel/Application/Readers
    qscc/GetChainInfo: /Channel/Application/Readers
    qscc/GetBlockByNumber: /Channel/Application/Readers
    qscc/GetBlockByHash: /Channel/Application/Readers
    qscc/GetTransactionByID: /Channel/Application/Readers
    qscc/GetBlockByTxID: /Channel/Application/Readers
    cscc/GetConfigBlock: /Channel/Application/Readers
    cscc/GetConfigTree: /Channel/Application/Readers
    cscc/SimulateConfigTreeUpdate: /Channel/Application/Readers
    peer/Propose: /Channel/Application/Writers
    peer/ChaincodeToChaincode: /Channel/Application/Readers
    event/Block: /Channel/Application/Readers
    event/FilteredBlock: /Channel/Application/Readers
    _lifecycle/CheckCommitReadiness: /Channel/Application/Writers
    _lifecycle/CommitChaincodeDefinition: /Channel/Application/Writers
    _lifecycle/QueryChaincodeDefinition: /Channel/Application/Readers
  Policies: &ApplicationDefaultPolicies
    Endorsement:
      Type: ImplicitMeta
      Rule: "ANY Endorsement"
    Readers:
      Type: ImplicitMeta
      Rule: "ANY Readers"
    Writers:
      Type: ImplicitMeta
      Rule: "ANY Writers"
    Admins:
      Type: ImplicitMeta
      Rule: "ANY Admins"
    LifecycleEndorsement:
      Type: ImplicitMeta
      Rule: "ANY Endorsement"
  Organizations:
  Capabilities:
    <<: *ApplicationCapabilities

# CHANNEL
Channel: &ChannelDefaults
  Policies:
    Readers:
      Type: ImplicitMeta
      Rule: "ANY Readers"
    Writers:
      Type: ImplicitMeta
      Rule: "ANY Writers"
    Admins:
      Type: ImplicitMeta
      Rule: "ANY Admins"
  Capabilities:
    <<: *ChannelCapabilities

# PROFILES
Profiles:
  CordychainOrdererGenesis:
    <<: *ChannelDefaults
    Orderer:
      <<: *OrdererDefaults
      Organizations:
        - <<: *Orderer
    Consortiums:
      CordychainConsortium:
        Organizations:
          - <<: *cordychain
          - <<: *BCEL
          - <<: *Harvesters
          - <<: *RegulatoryBody
    Policies:
      Readers:
        Type: ImplicitMeta
        Rule: "ANY Readers"
      Writers:
        Type: ImplicitMeta
        Rule: "ANY Writers"
      Admins:
        Type: ImplicitMeta
        Rule: "MAJORITY Admins"
      Endorsement:
        Type: ImplicitMeta
        Rule: "MAJORITY Endorsement"
    Application:
      <<: *ApplicationDefaults
      Organizations:
        - <<: *cordychain
        - <<: *BCEL
        - <<: *Harvesters
        - <<: *RegulatoryBody
      Policies:
        Readers:
          Type: ImplicitMeta
          Rule: "ANY Readers"
        Writers:
          Type: ImplicitMeta
          Rule: "ANY Writers"
        Admins:
          Type: ImplicitMeta
          Rule: "ANY Admins"
        Endorsement:
          Type: ImplicitMeta
          Rule: "MAJORITY Endorsement"

  CordychainChannel:
    <<: *ChannelDefaults
    Consortium: CordychainConsortium
    Application:
      <<: *ApplicationDefaults
      Organizations:
        - <<: *cordychain
        - <<: *BCEL
        - <<: *Harvesters
        - <<: *RegulatoryBody

  CordyChain_BCEL_Channel:
    Consortium: CordychainConsortium
    Policies:
      Readers:
        Type: ImplicitMeta
        Rule: "ANY Readers"
      Writers:
        Type: ImplicitMeta
        Rule: "ANY Writers"
      Admins:
        Type: ImplicitMeta
        Rule: "MAJORITY Admins"
      Endorsement:
        Type: ImplicitMeta
        Rule: "MAJORITY Endorsement"
    Application:
      Organizations:
        - *cordychain
        - *BCEL
      Policies:
        Readers:
          Type: ImplicitMeta
          Rule: "ANY Readers"
        Writers:
          Type: ImplicitMeta
          Rule: "ANY Writers"
        Admins:
          Type: ImplicitMeta
          Rule: "MAJORITY Admins"
        Endorsement:
          Type: ImplicitMeta
          Rule: "MAJORITY Endorsement"

  Harvester_BCEL_Channel:
    Consortium: CordychainConsortium
    Policies:
      Readers:
        Type: ImplicitMeta
        Rule: "ANY Readers"
      Writers:
        Type: ImplicitMeta
        Rule: "ANY Writers"
      Admins:
        Type: ImplicitMeta
        Rule: "MAJORITY Admins"
      Endorsement:
        Type: ImplicitMeta
        Rule: "MAJORITY Endorsement"
    Application:
      Organizations:
        - *Harvesters
        - *BCEL
      Policies:
        Readers:
          Type: ImplicitMeta
          Rule: "ANY Readers"
        Writers:
          Type: ImplicitMeta
          Rule: "ANY Writers"
        Admins:
          Type: ImplicitMeta
          Rule: "MAJORITY Admins"
        Endorsement:
          Type: ImplicitMeta
          Rule: "MAJORITY Endorsement"

  BCEL_RegulatoryBody_Channel:
    Consortium: CordychainConsortium
    Policies:
      Readers:
        Type: ImplicitMeta
        Rule: "ANY Readers"
      Writers:
        Type: ImplicitMeta
        Rule: "ANY Writers"
      Admins:
        Type: ImplicitMeta
        Rule: "MAJORITY Admins"
      Endorsement:
        Type: ImplicitMeta
        Rule: "MAJORITY Endorsement"
    Application:
      Organizations:
        - *BCEL
        - *RegulatoryBody
      Policies:
        Readers:
          Type: ImplicitMeta
          Rule: "ANY Readers"
        Writers:
          Type: ImplicitMeta
          Rule: "ANY Writers"
        Admins:
          Type: ImplicitMeta
          Rule: "MAJORITY Admins"
        Endorsement:
          Type: ImplicitMeta
          Rule: "MAJORITY Endorsement"


##############################################################################################################################################

Generate Network Artifacts
To generate the network artifacts, follow these steps:


run cryptogen generate --config=./crypto-config.yaml

This command will create TLS certificates and cryptographic key pairs for each organization and node in the network.
Generate Genesis Block: Use the configtxgen tool to create the genesis block, which initializes the ordering service and defines the initial configuration of the network:

cconfigtxgen -outputBlock ./orderer/cordychaingenesis.block -channelID ordererchannel -profile CordychainOrdererGenesis

Create Channel Configuration Transactions: Generate channel configuration transactions for each channel in the network using the following command:

configtxgen -profile cordychainchannel -outputCreateChannelTx ./channel-artifacts/cordychainchannel.tx -channelID cordychainchannel


##############################################################################################################################################
create the private channels 

configtxgen -profile CordyChain_BCEL_Channel -outputCreateChannelTx CordyChain_BCEL_Channel.tx -channelID CordyChain_BCEL_Channel

configtxgen -profile Harvester_BCEL_Channel -outputCreateChannelTx Harvester_BCEL_Channel.tx -channelID Harvester_BCEL_Channel

configtxgen -profile BCEL_RegulatoryBody_Channel -outputCreateChannelTx BCEL_RegulatoryBody_Channel.tx -channelID BCEL_RegulatoryBody_Channel


##############################################################################################################################################
Accessing Services
After the network is successfully started, you can access the services provided by the nodes:

Orderer: The orderer node can be accessed at localhost:7050.
Peers: Each peer node provides services at the specified ports:
bt.cordychain.com: localhost:7051
bt.bcel.com: localhost:8051
peer1.cordychain.com: localhost:9051
vil.Harvesters.com: localhost:10051
gov.RegulatoryBody.com: localhost:11051